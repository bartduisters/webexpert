(function () {
    'use strict';
    // 1. Module definieren
    angular.module('myApp', ['ngRoute'])
        .config(moduleConfig);

    // 2. Inject dependencies
    moduleConfig.$inject = ['$routeProvider'];

    // 3. Routes configureren
    function moduleConfig($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'views/H4_oef1/home.html',
            controller: 'homeController',
            controllerAs: 'homeCtrl'
        })
            .when('/home/', {
                templateUrl: 'views/H4_oef1/home.html',
                controller: 'homeController',
                controllerAs: 'homeCtrl'
            })
            .when('/detail/:id', {
                templateUrl: 'views/H4_oef1/detail.html',
                controller: 'detailController',
                controllerAs: 'detailCtrl'
            })
            .when('/partners/', {
                templateUrl: 'views/H4_oef1/partners.html',
                controller: 'partnersController',
                controllerAs: 'partnersCtrl'
            })
            .when('/add/', {
                templateUrl: 'views/H4_oef1/add.html',
                controller: 'addController',
                controllerAs: 'addCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
})();
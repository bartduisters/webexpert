// homeController.js
(function () {
    'use strict';

    var vehicleApp = angular.module("vehicleApp", ['ng-slide-down']);

    vehicleApp.controller('vehicleController', [vehicleController]);

    function vehicleController() {
        var vm = this;
        vm.vehicles = [
            {
                id: 0,
                name: "car",
                parts: {
                    wheels: 4,
                    doors: 4
                }
            },
            {
                id: 1,
                name: "plane",
                parts: {
                    wings: 2,
                    doors: 2
                }
            },
            {
                id: 2,
                name: "boat",
                parts: {
                    doors: 1
                }
            }];
    }
})();

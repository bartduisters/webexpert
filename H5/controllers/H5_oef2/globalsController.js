(function () {
    'use strict';
    // Controller toevoegen aan de module
    angular.module('myApp')
        .controller('globalsController', globalsController)
        // GLOBALS via een aparte file koppelen werkte niet bij mij
        .constant('GLOBALS', {
            appName: 'AutoService APP',
            appVersion: '1.0.0',
            developer: 'Bart Duisters',
            website: 'http://bartduisters.com'
        });

    // Globals injecteren
    globalsController.$inject = ['GLOBALS'];
    function globalsController(GLOBALS) {
        var vm = this;

        // Gegevens uit constante halen en toekennen aan variabelen
        vm.appName = GLOBALS.appName;
        vm.appVersion = GLOBALS.appVersion;
        vm.developer = GLOBALS.developer;
        vm.website = GLOBALS.website;
    }
})();


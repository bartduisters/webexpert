// detailController.js
(function () {
    'use strict';

    angular.module('myApp')
        .controller('detailController', detailController);

    detailController.$inject = ['$routeParams', 'carFactory'];

    function detailController($routeParams, carFactory) {
        var vm = this;
        // Gegevens uit de factory ophalen en toekennen aan variabelen
        vm.cars = carFactory.getCars();

        // Specifieke wagen ophalen via Factory
        var id = $routeParams.id;
        vm.car = carFactory.getCar(id);
    }
})();


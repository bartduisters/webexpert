// homeController.js
(function () {
	'use strict';
	angular.module('myApp')
		.controller('homeController', homeController);

	// 2. Factory injecteren
	homeController.$inject = ['carFactory'];
	function homeController(carFactory) {
		var vm = this;

		// Gegevens uit de factory ophalen en toekennen aan variabelen
		vm.cars = carFactory.getCars();

		// Functie om via ng-click aan te roepen
		vm.deleteCar = function (id) {
			console.log("Home Controller: " + id);
			// Wissen via factory
			carFactory.deleteCar(id);
		}
	}
})();

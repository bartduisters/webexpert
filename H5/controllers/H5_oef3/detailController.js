// detailController.js
(function () {
    'use strict';

    angular.module('myApp')
        .controller('detailController', detailController);

    detailController.$inject = ['$routeParams', 'carService'];

    function detailController($routeParams, carService) {
        var vm = this;
        // Gegevens uit de factory ophalen en toekennen aan variabelen
        vm.cars = carService.getCars();

        // Specifieke wagen ophalen via Factory
        var id = $routeParams.id;
        vm.car = carService.getCar(id);
    }
})();


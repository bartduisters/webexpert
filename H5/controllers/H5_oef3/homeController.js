// homeController.js
(function () {
	'use strict';
	angular.module('myApp')
		.controller('homeController', homeController);

	// 2. Factory injecteren
	homeController.$inject = ['carService'];
	function homeController(carService) {
		var vm = this;

		// Gegevens uit de factory ophalen en toekennen aan variabelen
		vm.cars = carService.getCars();

		// Functie om via ng-click aan te roepen
		vm.deleteCar = function (id) {
			console.log("Home Controller: " + id);
			// Wissen via factory
			carService.deleteCar(id);
		}
	}
})();

// addController.js
(function () {
	'use strict';
	// 1. Controller toevoegen aan de module
	angular.module('myApp')
		.controller('addController', addController);

	addController.$inject = ['carService', '$location'];
	function addController(carService, $location) {
		var vm = this;

		// 1. Nieuwe car maken op basis van formulier.
		// ID is uiteraard niet bekend, wordt in de factory berekend.
		vm.addCar = function () {
			var newCar = {
				description: vm.car.description,
				price: vm.car.price,
				type: vm.car.type,
				verkeersbelasting: vm.car.verkeersbelasting,
				belasting: vm.car.belasting,
				verbruik: vm.car.verbruik
			};

			// 2. Toevoegen via factory
			carService.addCar(newCar);

			// 3. Terug/doorsturen naar de homepage
			$location.path('/');
		}
	}
})();

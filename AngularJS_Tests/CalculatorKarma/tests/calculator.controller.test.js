describe('calculator', function () {

    beforeEach(angular.mock.module('calculatorApp'));

    var $controller;

    beforeEach(angular.mock.inject(function(_$controller_){
        $controller = _$controller_;
    }));

    describe('sum', function () {
        it('1 + 1 should equal 2', function () {
            var controller = $controller('CalculatorController');
            var x = 1;
            var y = 2;
            controller.sum(x, y);
            expect(controller.z).toBe(3);
        });
        it('z should have default value of zero', function () {
            var controller = $controller('CalculatorController');
            expect(controller.z).toBe(0);
        });
    });
});
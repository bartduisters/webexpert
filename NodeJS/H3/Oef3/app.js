// Wanneer je deze zelf aan het maken bent, doe je npm init

// je voegt nu 'colors' en 'underscore' toe aan de gegenereerde JSON

/*

 "dependencies": {
 "colors": "^1.1.0",
 "underscore": "^1.0.0"
 }

 */

// Nu voeg je 'npm install' uit via de terminal in de root van deze oefening
// Indien je deze oefening niet vanaf 0 start, dien je enkel 'npm install' te doen
var Persoon = require('./Persoon');
var colors = require('colors');
var underscore = require('underscore');

var personen = [];

// Personen toevoegen aan de array 'personen'
personen.push(new Persoon('Bart', 'Duisters', 'bartduisters@bartduisters.com'));
personen.push(new Persoon('Jeroen', 'Roumen', 'info@ikbennederlands.nl'));
personen.push(new Persoon('Evi', 'van Looy', 'ik@vindbartcool.com'));
personen.push(new Persoon('Luc', 'Doumen', 'ik@vindbartookcool.com'));

personen.forEach(function (persoon) {
    console.log('Naam: ', persoon.voornaam, persoon.achternaam);
    console.log(colors.blue.underline(persoon.email));
    console.log("-----------------------");
});
console.log("*************************");

console.log("EERSTE PERSOON IN DE ARRAY");
console.log(underscore.first(personen));
console.log("*************************");
console.log("LAATSTE PERSOON IN DE ARRAY");
console.log(underscore.last(personen));
console.log("*************************");

var sortedPersonen = underscore.sortBy(personen, 'voornaam');
sortedPersonen.forEach(function (persoon) {
    console.log('Naam: ', persoon.voornaam, persoon.achternaam);
    console.log(colors.blue.underline(persoon.email));
    console.log("-----------------------");
});
console.log("*************************");
var kwadraat = require('./kwadraat');
myKwadraat = new kwadraat();

var arrayNummers = [];
var amount = 10000;

// array vullen met personen
for (var i=0; i < amount; i++) {
	arrayNummers.push(i+1); // kwadraat van 1 tot en met 10000
}

// persoonsarray naar de console schrijven
console.time('Tijd om 100000 kwadraten te berekenen'); // Zorg dat deze string !!!

arrayNummers.forEach(function (nummer) {
	console.log(myKwadraat.getKwadraat(nummer));
});

console.timeEnd('Tijd om 100000 kwadraten te berekenen'); // Gelijk is aan deze string !!!
// Uitwerking oefening 5.6, boek toevoegen via een formulier op de website
var router = require('express').Router();
var http = require('http');
var wagens = require('../data/cars.json');
var types = require('../data/types.json');

// Dit moet er staan wanneer node server en je eigen server op verschillende
// poorten draaien
router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
// Tot hier

router.get('/api', function (req, res) {
	var msg = '<h1>Express API</h1>';
	msg += '<p>Gebruik \\api\\cars voor een lijst met wagens.</p>';
	msg += '<p>Gebruik \\api\\types voor een lijst van types.</p>';
	res.send(msg);
});

// Retourneer wagens
router.get('/api/wagens', function (req, res) {
	res.json(wagens);
});

// Retourneer types
router.get('/api/types', function (req, res) {
	res.json(types);
});

// Retourneer boeken
router.get('/api/boeken', function (req, res) {
	res.json(boeken);
});

// Retourneer specifiek boek, op basis van ID
router.get('/api/wagen/:id', function (req, res) {
	var id = req.params.id;
	var gezochteWagen;
	(wagens.records).forEach(function (wagen) {
		if (parseInt(wagen.Id) === parseInt(id)) {
			gezochteWagen = wagen;
		}
	});
	// Wagen niet gevonden
	if (!gezochteWagen) {
		gezochteWagen = {
			error: 'Wagen niet gevonden'
		}
	}
	res.json(gezochteWagen);
});

module.exports = router;
(function () {
	'use strict';

	angular.module('myApp')
		.controller('nummerController', nummerController);


    function nummerController() {
        var vm = this;

        var nummerIndex = 0;
        vm.nummers = [];

        vm.nieuwNummer = function () {
            nummerIndex += 1;
            vm.nummers.push(nummerIndex);
        };

        vm.verwijderNummer = function () {
            vm.nummers.splice(0,1);
        };
    }
})();
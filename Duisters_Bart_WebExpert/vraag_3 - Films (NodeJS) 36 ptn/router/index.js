var router = require('express').Router();
var films = require('../data/films.json');

// Dit moet er staan wanneer node server en je eigen server op verschillende
// poorten draaien
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
/* /Tot hier */

router.get('/api', function (req, res) {
    var msg = '<h1>Express API</h1>';
    msg += '<p>GET /api/films - Geeft een JSON met films.</p>';
    msg += '<p>POST /api/addfilm - Voeg film toe.</p>';
    msg += '<p>DELETE /api/deletefilm/:id - Wis film.</p>';
    res.send(msg);
});

/* GET /api/films */
router.get('/api/films', function (req, res) {
    res.json(films);
});

/* POST /api/automerk */
router.post('/api/addfilm', function (req, res) {
    var film = req.body;
    film.id = films.length + 1; // Quick and dirty ...
    films.push(film);
    res.json(films);
});

/* DELETE /api/automerk/:merknaam */
router.delete('/api/deletefilm/:id', function (req, res) {
    var id = req.params.id;
    var filmIndex = -1;
    for (var i = 0; i < films.length; i++) {
        if (films[i].id == id) {
            filmIndex = i;
        }
    }
    films.splice(filmIndex, 1);
    res.json(films);
});

module.exports = router;
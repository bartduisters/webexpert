var express = require('express');
var bodyParser = require('body-parser');
var routes = require('./router');
var app = express();

/* Enkel nodig wanneer je met meer dan GET calls werkt. */
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

/* Gebruik de map public om de HTML-pagina's te laden. */
app.use(express.static(__dirname + '/public'));
app.use(routes);
app.listen(3000, function(){
    console.log('Server running on localhost:3000');
});
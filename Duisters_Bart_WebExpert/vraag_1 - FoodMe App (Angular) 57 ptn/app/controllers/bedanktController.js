(function () {
    'use strict';

    angular.module('myApp')
        .controller('bedanktController', bedanktController);

    bedanktController.$inject = ['GLOBALS'];
    function bedanktController(GLOBALS) {
        var vm = this;
        vm.orderId = GLOBALS.orderId;
        vm.verhoogOrderId = function () {
            GLOBALS.orderId++;
        };
        vm.verhoogOrderId();
    }
})();
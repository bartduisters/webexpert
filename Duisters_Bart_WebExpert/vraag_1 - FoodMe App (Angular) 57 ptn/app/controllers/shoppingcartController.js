(function () {
    'use strict';

    angular.module('myApp')
        .controller('shoppingcartController', shoppingcartController);

    shoppingcartController.$inject = ['keuzeService'];

    function shoppingcartController(keuzeService) {
        var vm = this;

        vm.gerechten = [];
        vm.persoongegevens = [];
        vm.totaalprijs = 0;

        vm.getGekozenMenu = function () {
            if (keuzeService.getGekozenMenu() != null)
                vm.gerechten = keuzeService.getGekozenMenu();
        };
        vm.getGekozenMenu();

        vm.getPersoongegevens = function () {
            vm.persoongegevens = keuzeService.getAanmeldgegevens();
        };
        vm.getPersoongegevens();

        vm.berekenTotaalprijs = function () {
            vm.totaalprijs = 0;
            if (vm.gerechten === null) {
                vm.totaalprijs = 0;
            } else {
                for (var i = 0; i < vm.gerechten.length; i++) {
                    vm.totaalprijsVanDitGerecht = vm.gerechten[i].prijs * vm.gerechten[i].aantal;
                    vm.totaalprijs += vm.totaalprijsVanDitGerecht;
                }
            }


        };
        vm.berekenTotaalprijs();

        vm.maakLeeg = function () {
            vm.gerechten = [];
            keuzeService.setGekozenMenu(vm.gerechten);
            vm.getGekozenMenu();
            vm.berekenTotaalprijs();
        }
    }
})();
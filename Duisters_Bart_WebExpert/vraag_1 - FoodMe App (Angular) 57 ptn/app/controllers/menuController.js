(function () {
    'use strict';

    angular.module('myApp')
        .controller('menuController', menuController);

    menuController.$inject = ['menuFactory', 'keuzeService'];

    function menuController(menuFactory, keuzeService) {
        var vm = this;

        vm.menu = [];
        vm.restaurant = [];

        vm.gekozenGerechten = [];
        vm.totaalprijs = 0;

        // Ik doe niks met errorMsg en status in deze oefening omdat het niet gevraagd is.
        vm.errorMsg = '';
        vm.status = '';

        menuFactory.getMenu()
            .then(function (response) {
                vm.menu = response.data;
            })
            .catch(function (response) {
                vm.errorMsg = response.statusText;
                vm.status = response.status;
            });

        vm.getRestaurant = function () {
            vm.restaurant = keuzeService.getRestaurant();
        };
        vm.getRestaurant();

        vm.gerechtGekozen = function (gerecht) {
            for (var i = 0; i < vm.gekozenGerechten.length; i++) {
                if (vm.gekozenGerechten[i].menu == gerecht.menu) {
                    vm.gekozenGerechten[i].aantal++;
                    vm.berekenTotaalprijs();
                    return;
                }
            }
            gerecht.aantal = 1;
            vm.gekozenGerechten.push(gerecht);
            vm.berekenTotaalprijs();
        };

        vm.berekenTotaalprijs = function () {
            vm.totaalprijs = 0;
            for (var i = 0; i < vm.gekozenGerechten.length; i++) {
                vm.totaalprijsVanDitGerecht = vm.gekozenGerechten[i].prijs * vm.gekozenGerechten[i].aantal;
                vm.totaalprijs += vm.totaalprijsVanDitGerecht;
            }
            vm.setGekozenMenu();
        };

        vm.verminderAantal = function (gerecht) {
            for (var i = 0; i < vm.gekozenGerechten.length; i++) {
                if (vm.gekozenGerechten[i].menu == gerecht.menu) {
                    vm.gekozenGerechten[i].aantal--;
                    if (vm.gekozenGerechten[i].aantal < 1) {
                        vm.gekozenGerechten.splice(i, 1);
                    }
                    vm.berekenTotaalprijs();
                    return;
                }
            }
        };

        vm.setGekozenMenu = function () {
            keuzeService.setGekozenMenu(vm.gekozenGerechten);
        };

        vm.gekozenGerechtenLaden = function () {
            vm.gekozenGerechten = keuzeService.getGekozenMenu();
            vm.berekenTotaalprijs();
        };
        vm.gekozenGerechtenLaden();
    }
})();
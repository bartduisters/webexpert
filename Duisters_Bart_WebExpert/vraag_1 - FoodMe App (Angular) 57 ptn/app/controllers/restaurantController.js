(function () {
    'use strict';

    angular.module('myApp')
        .controller('restaurantController', restaurantController);

    restaurantController.$inject = ['restaurantFactory', 'keuzeService'];

    function restaurantController(restaurantFactory, keuzeService) {
        var vm = this;

        vm.restaurants = [];

        // Ik doe niks met errorMsg en status in deze oefening omdat het niet gevraagd is.
        vm.errorMsg = '';
        vm.status = '';

        restaurantFactory.getRestaurants()
            .then(function (response) {
                vm.restaurants = response.data;
            })
            .catch(function (response) {
                vm.errorMsg = response.statusText;
                vm.status = response.status;
            });

        vm.setRestaurant = function (restaurant) {
            keuzeService.setRestaurant(restaurant);
        }
    }
})();
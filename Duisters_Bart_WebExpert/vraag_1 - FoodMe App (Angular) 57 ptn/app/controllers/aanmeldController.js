(function () {
    'use strict';

    angular.module('myApp')
        .controller('aanmeldController', aanmeldController);

    aanmeldController.$inject = ['keuzeService'];

    function aanmeldController(keuzeService) {
        var vm = this;

        vm.gebruikersnaam = '';
        vm.leveringsadres = '';
        vm.telefoon = '';

        vm.setAanmeldgegevens = function () {
            var aanmeldgegevens = {
                gebruikersnaam: vm.gebruikersnaam,
                leveringsadres: vm.leveringsadres,
                telefoon: vm.telefoon
            };
            keuzeService.setAanmeldgegevens(aanmeldgegevens);
        }
    }
})();
(function () {
	'use strict';

    angular.module('myApp')
        .constant('GLOBALS', {
            menusUrl: 'data/menus.json',
			restaurantsUrl: 'data/restaurants.json',
            orderId: 1
        });
})();
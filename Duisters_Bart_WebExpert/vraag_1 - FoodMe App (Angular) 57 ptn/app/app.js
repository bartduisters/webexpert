(function () {
    'use strict';

    angular.module('myApp', ['ngRoute'])
        .config(moduleConfig);

    moduleConfig.$inject = ['$routeProvider'];

    function moduleConfig($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/aanmelden.html',
                controller: 'aanmeldController',
                controllerAs: 'aanmeldCtrl'
            })
            .when('/aanmelden', {
                templateUrl: 'views/aanmelden.html',
                controller: 'aanmeldController',
                controllerAs: 'aanmeldCtrl'
            })
            .when('/restaurants', {
                templateUrl: 'views/restaurants.html',
                controller: 'restaurantController',
                controllerAs: 'restaurantCtrl'
            })
            .when('/menu', {
                templateUrl: 'views/menu.html',
                controller: 'menuController',
                controllerAs: 'menuCtrl'
            })
            .when('/shoppingcart', {
                templateUrl: 'views/shoppingcart.html',
                controller: 'shoppingcartController',
                controllerAs: 'shoppingcartCtrl'
            })
            .when('/over-ons', {
                templateUrl: 'views/over_ons.html'
            })
            .when('/bedankt', {
                templateUrl: 'views/bedankt.html',
                controller: 'bedanktController',
                controllerAs: 'bedanktCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
})();
(function () {
	'use strict';

    angular.module('myApp')
        .factory('restaurantFactory', restaurantFactory);

    restaurantFactory.$inject = ['$http', 'GLOBALS'];
    function restaurantFactory($http, GLOBALS) {
        var factory = {};

        factory.getRestaurants = function () {
            return $http({
                method: 'GET',
                url: GLOBALS.restaurantsUrl
            });
        };

        return factory;
    }

})();
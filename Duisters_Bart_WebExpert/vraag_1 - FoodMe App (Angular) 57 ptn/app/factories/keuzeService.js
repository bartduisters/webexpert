(function () {
    'use strict';

    angular.module('myApp')
        .service('keuzeService', keuzeService);

    function keuzeService() {
        var service = {};

        var aanmeldgegevens;
        var restaurant;
        var menu = [];

        service.setAanmeldgegevens = function (gegevens) {
            console.log("service.setAanmeldgegevens " + JSON.stringify(gegevens));
            aanmeldgegevens = gegevens;
        };

        service.getAanmeldgegevens = function () {
            return aanmeldgegevens;
        };

        service.setRestaurant = function (gekozenRestaurant) {
            console.log("service.setRestaurant " + JSON.stringify(gekozenRestaurant));
            restaurant = gekozenRestaurant;
        };

        service.getRestaurant = function () {
            return restaurant;
        };

        service.setGekozenMenu = function (gekozenMenu) {
            console.log("service.setMenu " + JSON.stringify(gekozenMenu));
            menu = gekozenMenu;
        };

        service.getGekozenMenu = function () {
            return menu;
        };

        return service;
    }

})();
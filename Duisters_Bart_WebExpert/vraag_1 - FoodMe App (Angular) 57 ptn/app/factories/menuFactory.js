(function () {
	'use strict';

    angular.module('myApp')
        .factory('menuFactory', menuFactory);

    menuFactory.$inject = ['$http', 'GLOBALS'];
    function menuFactory($http, GLOBALS) {
        var factory = {};

        factory.getMenu = function () {
            return $http({
                method: 'GET',
                url: GLOBALS.menusUrl
            });
        };

        return factory;
    }

})();
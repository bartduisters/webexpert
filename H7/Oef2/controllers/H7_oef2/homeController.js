// homeController.js
(function () {
    'use strict';
    angular.module('myApp')
        .controller('homeController', homeController);

    // 2. Factory injecteren
    homeController.$inject = ['carFactory'];
    function homeController(carFactory) {
        var vm = this;
        vm.cars = [];
        vm.types = [];
        vm.errorMsg = '';
        vm.status = '';

        // Gegevens uit de factory ophalen en toekennen aan variabelen
        carFactory.getCars()
            .success(function (records) {
                vm.cars = records;
            })
            .error(function (err, status) {
                vm.errorMsg = err;
                vm.status = status;
            });

        carFactory.getTypes()
            .success(function (records) {
                vm.types = records;
            })
            .error(function (err, status) {
                vm.errorMsg = err;
                vm.status = status;
            });

        // Functie om via ng-click aan te roepen
        vm.deleteCar = function (id) {
            // console.log("Home Controller: " + id);
            // Wissen via factory
            carFactory.deleteCar(id);
        };

        // TODO: onderstaande nest werkende krijgen
        vm.typeChoice = "*"; /* Default = alles */
        vm.chosenType = "*"; /* Default = alles */
        vm.priceChoice = "*"; /* Default = niks */
        vm.priceLow = 0; /* laagste prijs */
        vm.priceHigh = 10000000000; /* hoogste prijs */
        // Functie via ng-click aan te roepen
        vm.setFilter = function () {
            console.log(vm.priceChoice);
            switch (vm.priceChoice) {
                case '1':
                    vm.priceLow = 20000;
                    vm.priceHigh = 30000;
                    break;
                case '2':
                    console.log("I");
                    vm.priceLow = 30000;
                    vm.priceHigh = 50000;
                    break;
                case '3':
                    vm.priceLow = 50000;
                    vm.priceHigh = 10000000000;
                    break;
                default:
                    vm.priceLow = 0;
                    vm.priceHigh = 10000000000;
                    break;
            }
            vm.chosenType = vm.typeChoice;
        };

        vm.priceFilter = function (item) {
            console.log(item);
            if (item.Basisprijs > vm.priceLow && item.Basisprijs < vm.priceHigh) {
                return item;
            }
            // return item;
        };

        vm.typeFilter = function (item) {
            if (vm.chosenType === "*") {
                return item;
            }
            if (vm.types.records[ item.TypeId - 1 ].Type === vm.chosenType) {
                return item;
            }
        }

    }
})();

// homeController.js
(function () {
    'use strict';
    angular.module('myApp')
        .controller('homeController', homeController);

    // Factory injecteren
    homeController.$inject = ['personFactory'];
    function homeController(personFactory) {
        var vm = this;
        vm.personChoice = ''; /* Wordt ingevuld wanneer changePerson wordt aangeroepen in HTML */
        vm.selectedPerson = [];

        // Gegevens uit de factory ophalen en toekennen aan variabelen
        personFactory.getPersons()
            .success(function (records) {
                // console.log(JSON.stringify(records));
                vm.persons = records;
            });

        vm.changePerson = function () {
            vm.showDetails = true;
            vm.displayDetails(vm.personChoice);
        };

        vm.displayDetails = function (personName) {
            for (var i = 0; i < vm.persons.length; i++) {
                var name = vm.persons[i].fname + ' ' + vm.persons[i].lname;
                name = name.replace(/ /g, '\u00a0'); /* vervangt alle spaties door &nbsp; */
                if (name === personName) {
                    vm.selectedPerson = vm.persons[i];
                }
            }
        }
    }
})();

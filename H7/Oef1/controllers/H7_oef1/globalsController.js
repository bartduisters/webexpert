(function () {
    'use strict';
    // Controller toevoegen aan de module
    angular.module('myApp')
        .controller('globalsController', globalsController)
        // GLOBALS via een aparte file koppelen werkte niet bij mij
        .constant('GLOBALS', {
            personsUrl: 'http://www.filltext.com/?rows=10&fname={firstName}&lname={lastName}&tel={phone|format}&email={email}'
        });

    // Globals injecteren
    globalsController.$inject = ['GLOBALS'];
    function globalsController(GLOBALS) {
        var vm = this;

        // Gegevens uit constante halen en toekennen aan variabelen
        vm.personUrl = GLOBALS.personsUrl;
    }
})();


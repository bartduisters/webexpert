(function () {
    'use strict';
    // 1. Module definieren
    angular.module('myApp', ['ngRoute'])
        .config(moduleConfig);

    // 2. Inject dependencies
    moduleConfig.$inject = ['$routeProvider'];

    // 3. Routes configureren
    function moduleConfig($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'views/H7_oef1/home.html',
            controller: 'homeController',
            controllerAs: 'homeCtrl'
        })
            .when('/home/', {
                templateUrl: 'views/H7_oef1/home.html',
                controller: 'homeController',
                controllerAs: 'homeCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }
})();
// personFactory.js
(function () {
	'use strict';

	// Definitie van de personFactory
	angular.module('myApp')
		.factory('personFactory', personFactory);

	personFactory.$inject = ['$http', 'GLOBALS'];
	// Implementatie van personFactory
	function personFactory($http, GLOBALS) {
		// 1. Definieer (leeg) factory-object
		var factory = {};
		
		// 3. Defineer een API met methods voor deze factory
		factory.getPersons = function () {
			return $http({
				method: 'GET',
				url: 'http://www.filltext.com/?rows=10&fname={firstName}&lname={lastName}&tel={phone|format}&email={email}'
			});
		};

		// 4. Tot slot: retourneer factory-object
		return factory;
	}
})();

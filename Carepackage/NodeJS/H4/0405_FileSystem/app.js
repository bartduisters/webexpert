// Voer dit uit door    node app.js   in de terminal te typen.
// Verschillende methoden om te werken met het 'file system'.

// Run één keer zonder dat text.txt bestaat.
// Run één keer nadat je text.txt hebt toegevoegd.

var fs = require('fs');
var msg = 'Hello World';

// #1. Bestand opslaan
fs.writeFile('hello.txt', msg, function () {
    console.log('Bestand opgeslagen!')
});

// #2. Bestand inlezen en in de console tonen
fs.readFile('hello.txt', 'utf8', function (err, data) {
    if (err) {
        console.log('Error: ', err);
    } else {
        console.log('Bestand ingelezen: ', data);
    }
});

// #3. Bestand verwijderen via unlink
fs.unlink('text.txt', function (err) {
    if (err) {
        console.log('Error: ', err);
    } else {
        console.log('text.txt is verwijderd');
    }
});

// #4. Eerst testen of bestand bestaat, voordat unlink wordt aangeroepen:
fs.exists('test.txt', function (exists) {
    if (exists) {
        // Zelfgemaakte functie hieronder.
        deleteFile('test.txt');
    } else {
        console.log('text.txt niet gevonden! Kan niet verwijderen.')
    }
});

function deleteFile(fileName) {
    fs.unlink(fileName, function (err) {
        if (err) {
            console.log('Error: ', err);
        } else {
            console.log(fileName, ' is verwijderd');
        }
    });
}


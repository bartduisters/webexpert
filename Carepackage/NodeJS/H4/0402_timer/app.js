// Voer dit uit door    node app.js   in de terminal te typen.

// #1 - Timer via een anonieme functie.
// Deze zal na 3 seconden de boodschap naar de log schrijven.
setTimeout(function () {
    console.log('Er zijn drie seconden verstreken!')
}, 3000);


// #2 - Timer via een benoemde functie.
// Deze zal na 2 seconden de boodschap naar de log schrijven.
setTimeout(showMsg, 2000);
function showMsg() {
    console.log('Nu zijn er twee seconden verstreken.')
}

// #3 - interval - deze functie wordt elke seconden aangeroepen.
var aantal = 0;
var interval = setInterval(function () {
    aantal += 1;
    console.log(aantal, 'seconden verstreken.');
    // Na 5 seconden stoppen we de interval
    if (aantal === 5) {
        console.log('Exit na vijf seconden.');
        clearInterval(interval); // Dit is nodig om de interval te stoppen, anders gaat hij gewoon door!
    }
}, 1000);
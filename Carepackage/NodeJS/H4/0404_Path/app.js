// Voer dit uit door    node app.js   in de terminal te typen.
// Bekijk de log in de console om te zien wat elke functie doet.

var path = require('path');
var voorbeeldPath = ('home/bart/Documents/test.html');
console.log('normalize: ', path.normalize(voorbeeldPath));
console.log('resolve: ', path.resolve(voorbeeldPath));
console.log('dirname: ', path.dirname(voorbeeldPath));
console.log('basename: ', path.basename(voorbeeldPath));
console.log('extname: ', path.extname(voorbeeldPath));


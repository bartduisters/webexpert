// Voer dit uit door    node server_02.js   in de terminal te typen.
// Dit is grotendeels gelijkend op de vorige oefening, enkel de nieuwe dingen zijn voorzien van commentaar.

var http = require('http'),
	fs = require('fs'),
	path = require('path'),
	mime = require('mime'), // Gebruik de module 'mime' om correcte MIME-types te gebruiken.
	root = __dirname + '/public/';

var server = http.createServer(function (req, res) {
	var fileName = '';
	var url = req.url;
	if (url === '/') {
		url = 'index.html';
	}
	fileName = root + url;
	console.log('Gevraagd bestand: ', path.basename(fileName));

	fs.exists(fileName, function (exists) {
		if (exists) {
			serveFile(fileName);
		} else {
			fileName = root + '404.html';
			serve404(fileName);
		}
	});

	function serveFile(requestFile) {
		// Het correcte type MIME wordt opgezocht met de functionaliteit van de module 'mime'.
		res.writeHead(200, {'Content-Type': mime.lookup(requestFile)});
		var stream = fs.createReadStream(requestFile);
		stream.on('data', function (chunk) {
			res.write(chunk);
		});
		stream.on('end', function () {
			res.end();
		});
		stream.on('error', function (err) {
			console.log('error: ' + err);
		});
	}

	function serve404(requestFile) {
		// Het correcte type MIME wordt opgezocht met de functionaliteit van de module 'mime'.
		res.writeHead(404, {'Content-Type': mime.lookup(requestFile)});
		fs.readFile(requestFile, 'utf8', function (err, data) {
			if (err) {
				console.log('Error: ', err);
			} else {
				res.end(data);
			}
		})
	}
});

// 2. Start de server
server.listen(3000, function () {
	console.log('Server gestart op http://localhost:3000');
});

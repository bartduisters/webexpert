// Voer dit uit door    node server_01.js   in de terminal te typen.
// Vul manueel een niet bestaand bestand in achter de URL om de 404-pagina te zien.

var http = require('http'),
    fs = require('fs'), // File System (om bestanden te kunnen lezen/maken/wissen)
    path = require('path'), // Path (om de padenfuncties te kunnen gebruiken)
    root = __dirname + '/public/'; // __dirname is een 'magic variabele'

var server = http.createServer(function (req, res) {
    var fileName = '';
    var url = req.url; // Haal de URL op uit de request.
    // Controleer of de root wordt opgevraagd.
    if (url === '/') {
        url = 'index.html'; // Redirect als er geen bestandsnaam is opgegeven.
    }
    fileName = root + url; // fileName bevat nu __dirname + /public/ + url
    console.log('Gevraagd bestand: ', path.basename(fileName));

    // Controleer of het bestand bestaat.
    fs.exists(fileName, function (exists) {
        if (exists) {
            // serveFile is een zelfgemaakte functie, zie hieronder.
            serveFile(fileName); // Het bestaat (in de map 'public'), toon het gevraage bestand.
        } else {
            fileName = root + '404.html'; // Het bestaat niet (in de map 'public'), dus wordt de bestaande '404.html' toegevoegd.
            serveFile(fileName); // Toon de 404.html.
        }
    });

    // Serveer het gevraagde bestand.
    function serveFile(requestFile) {
        // Maak een stream en server op basis van Events
        var stream = fs.createReadStream(requestFile);
        // De gegevens worden in 'stukjes' (chunks) opgehaald. Zolang er nieuwe stukjes zijn, blijf deze toevoegen.
        stream.on('data', function (chunk) {
            res.write(chunk);
        });
        // Wanneer er geen nieuwe stukjes meer zijn, eindigt het.
        stream.on('end', function () {
            res.end();
        });
        // Wanneer er een error ontstaat, log deze.
        stream.on('error', function (err) {
            console.log('error: ' + err);
        });
    }
});

server.listen(3000);
console.log('Server gestart op http://localhost:3000');

// Voer dit uit door    node app.js   in de terminal te typen.

// Verschillende logmeldingen.
console.log('Algemene logging-melding');
console.info('Algemene informatie-melding');
console.error('Algemene foutmelding');
console.warn('Algemene waarschuwing');

// De tekst tussen de quotes van console.time moet identiek zijn aan de tekst tussen de quest van console.timeEnd!
console.time('Tijdsduur van for-loop');
var j = 0;
for (var i = 0; i < 10000000; i += 1) {
    j += i;
}
console.log('j: ', j);
console.timeEnd('Tijdsduur van for-loop');


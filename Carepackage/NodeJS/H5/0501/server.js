// Voer dit uit door    node server.js   in de terminal te typen.
// Je merkt dat na een	npm install		er heel wat meer wordt geïnstalleerd dan voorheen.
// Dit heeft te maken met al de dependencies van de module 'express'.

var express = require('express');
var app = express();

// Alle verschillende routes bestaan uit een	app.get		waarbij de eerste parameter de match van de URL
// is en de tweede parameter een functie waarin staat wat er moet gebeuren wanneer er een match is.

// Wanneer er een match is op '/'
app.get('/', function (req, res) {
	// Dit geeft een response met als tekst onderstaande string.
	res.send('Hello World - dit is Express!');
});


var persoon = {
	voornaam  : 'Bart',
	achternaam: 'Duisters'
};

// Wanneer er een match is op '/json', geven we het bovenstaande persoon-object terug.
app.get('/json', function (req, res) {
	res.send(persoon);
});

app.listen(3000);
console.log('Express-server gestart op http://localhost:3000');
// Ga in je browser naar http://localhost:3000/ en http://localhost:3000/json
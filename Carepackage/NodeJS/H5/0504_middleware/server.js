// Voer		bower install	uit zodat je de front-end dependencies hebt.
// Voer		npm install		uit zodat je de back-end dependencies hebt.
// Voer dit uit door    node server.js   in de terminal te typen.

// Enkel wat tussen /* NIEUW */ en /* /NIEUW */ staat is anders ten opzichte van de vorige oefeningen.
// Start de server, bezoek verschillende pagina's en kijk naar de log in de console.

var express = require('express');
var app = express();

var auteurs = require('./auteurs.json');
var boeken = require('./boeken.json');

/* NIEUW */

//*********************
// Eigen middleware-functie
//*********************
// Dit logt de URL van elke opgevraagde route.
app.use(function(req, res, next){
	console.log('Requested file: ', req.url);
	next();
});

//*********************
// Middleware-functie voor één route
//*********************
// Dit logt enkel wanneer /index.html wordt opgevraagd.
app.use('/index.html', function(req, res, next){
	console.log('Homepage opgevraagd: ', new Date());
	next();
});

/* /NIEUW */

// Stel middleware in voor serveren van statische bestanden (HTML, CSS, images), dit maakt gebruik van de module 'express'
app.use(express.static(__dirname + '/public'));



app.listen(3000);
console.log('Express app gestart op localhost:3000');

//**************************
// De Routes voor de API.
//**************************
// Elke app.get is een andere route.

// Match '/', geef statisch HTML terug.
app.get('/', function (req, res) {
	var msg = '<h1>Express API</h1>';
	msg += '<p>Gebruik \\api\\auteurs voor een lijst met auteurs.</p>';
	msg += '<p>Gebruik \\api\\boeken voor een lijst met boeken.</p>';
	msg += '<p>Gebruik \\api\\boek\\:id voor een specifiek boek.</p>';
	msg += '<p>Gebruik \\api\\auteur\\:id voor een specifieke auteur.</p>';
	res.send(msg);
});

// Match '/api/auteurs', geef de auteurs.json terug.
app.get('/api/auteurs', function (req, res) {
	res.json(auteurs);
});

// Match '/api/boeken', geef de boeken.json terug.
app.get('/api/boeken', function (req, res) {
	res.json(boeken);
});

// Match '/api/boek/:id', geef een specifiek boek terug op basis van een ID
app.get('/api/boek/:id', function (req, res) {
	var id = req.params.id; // ID ophalen uit de parameters van de request.
	var gezochtBoek;
	boeken.forEach(function (boek) {
		// Als het opgegeven ID overeenkomt met het ID van een boek uit de JSON.
		if (boek.id === parseInt(id)) {
			// Dan sla het boek op.
			gezochtBoek = boek;
		}
	});
	// Indien het boek niet gevonden is.
	if (!gezochtBoek) {
		gezochtBoek = {
			error: 'Boek niet gevonden'
		}
	}
	res.json(gezochtBoek);
});

// Match '/api/auteur/:id', geef een specifiek boek terug op basis van een ID
app.get('/api/auteur/:id', function (req, res) {
	var id = req.params.id; // ID ophalen uit de parameters van de request.
	var gezochteAuteur;
	boeken.forEach(function (auteur) {
		// Als het opgegeven ID overeenkomt met het ID van een boek uit de JSON.
		if (auteur.id === parseInt(id)) {
			// Dan sla het boek op.
			gezochteAuteur = auteur;
		}
	});
	// Indien het boek niet gevonden is.
	if (!gezochteAuteur) {
		gezochteAuteur = {
			error: 'Auteur niet gevonden'
		}
	}
	res.json(gezochteAuteur);
});

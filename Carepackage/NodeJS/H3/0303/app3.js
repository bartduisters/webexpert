// Voer dit uit door    node app3.js   in de terminal te typen.

// logger3.js heeft een require naar getTime.js, de tijd wordt nu ook getoond.
// Hier verandert er verder niks t.o.v. de vorige oefening.

var logger = require('./logger3');

var myLog = logger();
myLog.log(' Dit is een algemene logregel');
myLog.info(' Een informatieve melding');
myLog.error(' Een errormelding');

// Deze module geeft een geformatteerde tijd (hh:mm:ss) terug.

module.exports = function () {
	var now = new Date();
	return now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
};
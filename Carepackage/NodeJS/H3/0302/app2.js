// Voer dit uit door    node app2.js   in de terminal te typen.
// logger2.js exporteert een class met verschillende methoden (i.p.v. 3 aparte methoden).

var logger = require('./logger2');

var myLog = logger(); // functie invoking
myLog.log('Dit is een normale log');
myLog.info('Dit is informatie');
myLog.error('Dit is een foutmelding');

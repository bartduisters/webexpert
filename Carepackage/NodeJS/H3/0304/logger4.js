// Require de module 'moment'. Let op het ontbreken van de padinformatie.
// Node.js kijkt dan standaard in \node_modules (die aangemaakt wordt wanneer je een 'npm install' doet van een package.json).
var moment = require('moment');

// Nu wordt de module 'moment' gebruikt om de tijd/datum toe te voegen aan het logbericht.
module.exports = function () {
	moment.locale('nl');	// Stel Nederlandse notatie in
	this.log = function (msg) {
		console.log(moment().format('lll') + ', Log >> ' + msg);
	};

	this.info = function (msg) {
		console.info(moment().format('lll') + ', Info >> ' + msg);
	};

	this.error = function (msg) {
		console.error(moment().format('lll') + ', Error >> ' + msg);
	};

	return this;
};
// Doe een npm install vanuit de root van 0305. Dit installeert de nodige dependencies.
// Voer dit uit door    node app.js   in de terminal te typen.

// ./Persoon omdat het vanuit deze directory 'Persoon.js' moet inladen.
var Persoon = require('./Persoon');
// Geen './' omdat het standaard voor dependencies in node_modules gaat kijken.
var colors = require('colors');

// Array aanmaken.
var arrayPersonen = [];
// Array vullen met personen.
arrayPersonen.push(new Persoon('Bart', 'Duisters', 'bartduisters@bartduisters.com'));
arrayPersonen.push(new Persoon('Sylvia', 'De Vijlder', 'sylvia@devijlder.com'));
arrayPersonen.push(new Persoon('Jeroen', 'De Vijlder','jeroen@huphollandhup.com'));
arrayPersonen.push(new Persoon('Evi','Roumen','evi@vanlooy.com'));
arrayPersonen.push(new Persoon('Luc','Van Looy','luc.doumen@pxl.be'));
arrayPersonen.push(new Persoon('Linus', 'Doumen','linus@linux.com'));

// Elke persoon in de array loggen in de console.
arrayPersonen.forEach(function (persoon) {
	console.log('Naam: ', persoon.voornaam, persoon.achternaam);
	console.log(persoon.email.blue.underline); // Dit is functionaliteit van de 'colors'-module.
	// OF: console.log(colors.blue.underline(persoon.email));
	console.log('/*-------------------*/')
});

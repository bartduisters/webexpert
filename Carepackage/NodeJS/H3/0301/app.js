// Voer dit uit door    node app.js   in de terminal te typen.

// Importeer logger.js (./logger en ./logger.js kunnen beide).
var logger = require('./logger');

// Maak gebruik van de 3 methoden die in logger.js staan.
logger.log('Algemene melding');
logger.info('Dit is een informatieve melding');
logger.error('Dit is een foutmelding');
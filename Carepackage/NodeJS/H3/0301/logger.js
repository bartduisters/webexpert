// Exporteer verschillende algemene logmethoden.
// Deze zullen opgeroepen kunnen worden in de module die logger importeert.
// Elke methode wordt hier apart geëxporteerd (zie 0302/logger2.js voor alternatief).
module.exports.log = function(msg){
	console.log('>> Log: ' + msg);
};
module.exports.info = function(msg){
	console.info('>> Info: ' + msg);
};
module.exports.error = function(msg){
	console.error('>> Error: ' + msg);
};
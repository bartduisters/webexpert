// Doe een npm install vanuit de root van 0305. Dit installeert de nodige dependencies.
// Voer dit uit door    node app5.js   in de terminal te typen.

var logger = require('./logger5');

var myLog = logger();
myLog.log('Algemene logging-regels worden nu groen weergegeven');
myLog.info('Informatieve meldingen zijn blauw');
myLog.error('Foutmeldingen zijn rood en vet');

// Require de module 'moment' en 'colors'. Let op het ontbreken van de padinformatie.
// Node.js kijkt dan standaard in \node_modules (die aangemaakt wordt wanneer je een 'npm install' doet van een package.json).
var moment = require('moment'),
	colors = require('colors');

module.exports = function () {
	moment.locale('nl');	// Stel Nederlandse notatie in

	// Alles blijft hetzelfde, maar nu wordt de string eerst in een tijdelijke variabele opgeslagen.
	// Hierop wordt dan de functionaliteit van colors toegepast.
	this.log = function (msg) {
		var prefixString = moment().format('lll') + ', Log >> ';
		console.log(prefixString.green + msg);
	};

	this.info = function (msg) {
		var prefixString = moment().format('lll') + ', Info >> ';
		console.info(prefixString.blue + msg);
	};

	this.error = function (msg) {
		var prefixString = moment().format('lll') + ', Error >> ';
		console.error(prefixString.bold.red + msg);
	};

	return this;
};
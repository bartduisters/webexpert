// Voer dit uit door    node 06_server_json.js   in de terminal te typen.
// Een JSON-webserver.

var http = require('http');

var server = http.createServer(function (request, response) {

	// Content-Type op application/json zetten.
	response.writeHead(200, {'Content-Type' : 'application/json'});

	// Een JSON-string teruggeven (Let op: het MOET een valid JSON-string zijn)
	// Via https://jsonformatter.curiousconcept.com/ kan je controleren of jouw JSON valid is.
	response.write('{"voornaam": "Bart","achternaam": "Duisters", "email": "bartduisters@bartduisters.com"}');
	response.end();
});

server.listen(3000);
console.log('Server gestart op http://localhost:3000 ...');

// Installeer een JSON-viewer als extensie op je browser.
// Deze werkt op Firefox, Opera en Chrome: https://github.com/lauriro/json-lite
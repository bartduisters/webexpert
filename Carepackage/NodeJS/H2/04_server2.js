// Voer dit uit door    node 04_server2.js   in de terminal te typen.
// Na het starten van de server typ je iets achteraan de URL om het resultaat te zien.

var http = require('http');

var server = http.createServer(function (request, response) {
    var url = request.url;
    // Voeg response headers toe. 200 = success. Content-Type wordt op html gezet.
    response.writeHead(200, {'Content-Type':'text/html'});
    response.write('<h1>De gevraagde URL: ' + url + '</h1>');
    response.end();
});

server.listen(3000);
console.log('Server gestart op http://localhost:3000 ...');
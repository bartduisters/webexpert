// Voer dit uit door    node 03_server.js   in de terminal te typen.
// Maak een webser.

var http = require('http');

var server = http.createServer(function (request, response) {
	// Voeg response headers toe. 200 = success. Content-Type wordt op normale tekst gezet.
	response.writeHead(200, {'Content-Type':'text/plain'});
	// De tekst die geschreven moet worden.
	response.write('Hello World');
	// Response afsluiten.
	response.end();
});

server.listen(3000); // Server starten op poort 3000.
// Ga nu naar http://localhost:3000 in een browser naar keuze.
console.log('Server gestart op http://localhost:3000 ...');
// Voer dit uit door    node 05_server3.js   in de terminal te typen.
// Voer het script uit, open de browser en ga naar http://localhost:3000 en bekijk de console waarin
// je node gestart hebt. Hierin staan nu de Request Headers.
var http = require('http');

var server = http.createServer(function (request, response) {
	console.log(request.headers);
	//console.log(request.headers['user-agent']); // één specifieke header tonen
	var url = request.url;
	response.writeHead(200, {'Content-Type':'text/html'});
	response.write('<h1>De gevraagde URL: ' + url + '</h1>');
});

server.listen(3000);
console.log('Server gestart op http://localhost:3000 ...');

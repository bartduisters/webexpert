// Voer dit uit door    node 02_helloworld2.js   in de terminal te typen.

var msg = 'Hello World';
console.log(msg); // Geeft de inhoud van 'msg' weer.
console.log(68 + 1); // Geeft het resultaat van de som.

// Werken met objecten.
var persoon = {voornaam : 'Bart', achternaam : 'Duisters'};
console.log('Persoon: ' + persoon.voornaam + ' ' + persoon.achternaam);

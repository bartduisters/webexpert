// Voer dit uit door    node 07_server_json2.js   in de terminal te typen.
// Een JSON-webserver.

var http = require('http');

// Variabelen/Objecten van dit script.
var persoon = {
    voornaam   : 'Bart',
    achternaam : 'Duisters',
    email      : 'bartduisters@bartduisters.com'
};
var contact = {
    adres          : 'Sesamstraat 69',
    postcode       : '6969',
    plaats         : 'Hamont',
    openingstijden : 'Dagelijks van 9:00-17:00 uur'
};
var obj404 = {
    status : '404',
    text   : 'Niet gevonden ...'
};

// Server maken
var server = http.createServer(function (request, response) {
    // Wanneer de URL '/' of '/persoon' is, toon je het Persoon-object.
    if (request.url === '/persoon' || request.url === '/') {
        response.writeHead(200, {'Content-Type' : 'application/json'});
        response.write(JSON.stringify(persoon));
        // Wanneer de URL '/contact' is, toon je het Contact-object.
    } else if (request.url === '/contact') {
        response.writeHead(200, {'Content-Type' : 'application/json'});
        response.write(JSON.stringify(contact));
        // Default (voor alle niet gekende routes) laat je het obj404-object zien.
    } else {
        response.writeHead(404, {'Content-Type' : 'application/json'});
        response.write(JSON.stringify(obj404));
    }
    response.end();
});

server.listen(3000);
console.log('Server gestart op http://localhost:3000 ...');
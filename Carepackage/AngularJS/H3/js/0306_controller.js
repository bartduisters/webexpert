﻿// Dit bevat nu de controller
(function () {
    angular.module('myApp') // angular.module() fungeert nu als getter.
        .controller('myController',
            function ($scope) {
                // 1. array met gegevens
                $scope.lijstMetPersonen = [
                    {voornaam : 'Bart', achternaam:'Duisters', leeftijd: 25, email: 'bartduisters@bartduisters.com'},
                    {voornaam : 'Sylvia', achternaam:'De Vijlder', leeftijd: 24, email: 'sylvia@devijlder.com'},
                    {voornaam : 'Jeroen', achternaam:'Roumen', leeftijd: 24, email: 'jeroen@huphollandhup.com'},
                    {voornaam : 'Evi', achternaam:'Van Looy', leeftijd: 21, email: 'evi@vanlooy.com'},
                    {voornaam : 'Luc', achternaam:'Doumen', leeftijd: 51, email: 'luc.doumen@pxl.be'},
                    {voornaam : 'Linus', achternaam:'Torvalds', leeftijd: 47, email: 'linus@linux.com'}
                ];

                // 2. functie die gekoppeld zit aan ng-click
                $scope.toonDetails = function (persoon) {
                    $scope.voornaam = persoon.voornaam;
                    $scope.achternaam = persoon.achternaam;
                    $scope.leeftijd = persoon.leeftijd;
                    $scope.email = persoon.email;
                };

                // 3. controller initialiseren
                function init() {
                    $scope.toonDetails($scope.lijstMetPersonen[0]);
                }

                init();
            });
})();


//**********************************************************************
// alternatieve notatiewijze, door angular.module('myApp') mee te geven als parameter aan de iffy:
//(function (app) {	
//		app.controller('myController', function ($scope) {
// 1. array met gegevens
// $scope.lijstMetPersonen = [
//     {voornaam : 'Bart', achternaam:'Duisters', leeftijd: 25, email: 'bartduisters@bartduisters.com'},
//     {voornaam : 'Sylvia', achternaam:'De Vijlder', leeftijd: 24, email: 'sylvia@devijlder.com'},
//     {voornaam : 'Jeroen', achternaam:'Roumen', leeftijd: 24, email: 'jeroen@huphollandhup.com'},
//     {voornaam : 'Evi', achternaam:'Van Looy', leeftijd: 21, email: 'evi@vanlooy.com'},
//     {voornaam : 'Luc', achternaam:'Doumen', leeftijd: 51, email: 'luc.doumen@pxl.be'},
//     {voornaam : 'Linus', achternaam:'Torvalds', leeftijd: 47, email: 'linus@linux.com'}
// ];

// 2. functie die gekoppeld zit aan ng-click
// $scope.toonDetails = function (persoon) {
//     $scope.voornaam = persoon.voornaam;
//     $scope.achternaam = persoon.achternaam;
//     $scope.leeftijd = persoon.leeftijd;
//     $scope.email = persoon.email;
// };

//			// 3. controller initialiseren
//			function init() {
//				$scope.toonDetails($scope.lijstMetPersonen[0]);
//			}

//			init();
//		});
//})(angular.module('myApp'));
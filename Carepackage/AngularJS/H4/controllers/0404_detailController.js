// detailController.js
(function () {
	'use strict';

	angular.module('myApp')
		.controller('detailController', detailController);

	detailController.$inject = ['$routeParams'];

	// De lijst wordt hiet apart nog eens bijgehouden om specifiek één persoon eruit te kunnen halen hieronder.
	// Later wordt dit één JSON-bestand (of dus één API-call) dat door beide controllers wordt aangesproken.
	var lijstMetPersonen = [
            {id: 1, voornaam : 'Bart', achternaam:'Duisters', leeftijd: 25, email: 'bartduisters@bartduisters.com'},
            {id: 2, voornaam : 'Sylvia', achternaam:'De Vijlder', leeftijd: 24, email: 'sylvia@devijlder.com'},
            {id: 3, voornaam : 'Jeroen', achternaam:'Roumen', leeftijd: 24, email: 'jeroen@huphollandhup.com'},
            {id: 4, voornaam : 'Evi', achternaam:'Van Looy', leeftijd: 21, email: 'evi@vanlooy.com'},
            {id: 5, voornaam : 'Luc', achternaam:'Doumen', leeftijd: 51, email: 'luc.doumen@pxl.be'},
            {id: 6, voornaam : 'Linus', achternaam:'Torvalds', leeftijd: 47, email: 'linus@linux.com'}
        ];

	// De parameter wordt opgevraagd (zie 0404_app.js)
	// vm.persoon is beschikbaar in 0404_detail.html
	function detailController($routeParams) {
		var vm = this,
			id = $routeParams.id;
		vm.persoon = lijstMetPersonen[id - 1]; // -1 omdat array zero-based is en de IDs zijn dit niet.
	}
})();


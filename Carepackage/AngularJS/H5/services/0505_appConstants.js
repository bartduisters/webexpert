// AppConstants.js
(function () {
	'use strict';

	// Constanten in deze app
	angular.module('myApp', [])
		.constant('GLOBALS', {
			appNaam: 'Super Angular App',
			appVersie: '1.0.0',
			baseUrl: 'http://api.bartduisters.com',
			productUrl: '/producten',
			aboutUrl: '/over-mij',
			loginstatus: {
				login: 'Inloggen',
				loguit: 'Uitloggen'
			}
			// ... meer constanten
		});
})();
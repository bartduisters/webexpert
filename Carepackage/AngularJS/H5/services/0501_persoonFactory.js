// personFactory.js
(function () {
    // 'use strict' is om compatibel te zijn met oudere versies van JavaScript
    'use strict';

    // Voeg de factory toe aan de module.
    angular.module('myApp')
        .factory('persoonFactory', persoonFactory);

    // Implementatie van personFactory.
    function persoonFactory() {
        // 1. Definieer een (leeg) factory-object.
        var factory = {};

        // 2. Definieer data voor deze factory (later zullen deze gegevens uit een database komen).
        var lijstMetPersonen = [
            {id: 1, voornaam : 'Bart', achternaam:'Duisters', leeftijd: 25, email: 'bartduisters@bartduisters.com'},
            {id: 2, voornaam : 'Sylvia', achternaam:'De Vijlder', leeftijd: 24, email: 'sylvia@devijlder.com'},
            {id: 3, voornaam : 'Jeroen', achternaam:'Roumen', leeftijd: 24, email: 'jeroen@huphollandhup.com'},
            {id: 4, voornaam : 'Evi', achternaam:'Van Looy', leeftijd: 21, email: 'evi@vanlooy.com'},
            {id: 5, voornaam : 'Luc', achternaam:'Doumen', leeftijd: 51, email: 'luc.doumen@pxl.be'},
            {id: 6, voornaam : 'Linus', achternaam:'Torvalds', leeftijd: 47, email: 'linus@linux.com'}
        ];

        // 3. Defineer een API met methoden voor deze factory.
        // Geeft een lijst met alle personen terug.
        factory.getPersonen = function () {
            return lijstMetPersonen;
        };

        // Zoekt een specifieke persoon op basis van ID uit de lijst.
		// De id is de /:id uit de URL (zie 0502_app.js) die via de detailController is meegegeven (zie 0502_detailController.js)
        factory.getPersoon = function (id) {
            return lijstMetPersonen[id - 1]; // -1 omdat de array zero-based is
        };

        factory.addPersoon = function (newPersoon) {
            // De nieuwe persoon heeft nog geen ID.
            // Hier wordt op een quick & dirty manier de persoon met de hoogste ID gezocht en daaraan wordt 1 toegevoegd.
            var id = 0;
            for (var i = 0; i < lijstMetPersonen.length; i++) {
                if (lijstMetPersonen[i].id >= id) {
                    id = lijstMetPersonen[i].id + 1;
                }
            }
            // Ken het ID toe aan de nieuwe persoon.
            newPersoon.id = id;
            // Voeg de nieuwe persoon toe aan de lijst met personen.
            // Dit zal momenteel enkel in de huidige sessie werken, de gegevens worden namelijk niet opgeslagen.
            lijstMetPersonen.push(newPersoon);
        };

        // 4. Tot slot: retourneer factory-object.
        return factory;
    }
})();

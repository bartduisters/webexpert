// persoonService.js
(function () {
	'use strict';

	angular.module('myApp')
		.service('persoonService', persoonService);

	function persoonService() {

		var lijstMetPersonen = [
            {id: 1, voornaam : 'Bart', achternaam:'Duisters', leeftijd: 25, email: 'bartduisters@bartduisters.com'},
            {id: 2, voornaam : 'Sylvia', achternaam:'De Vijlder', leeftijd: 24, email: 'sylvia@devijlder.com'},
            {id: 3, voornaam : 'Jeroen', achternaam:'Roumen', leeftijd: 24, email: 'jeroen@huphollandhup.com'},
            {id: 4, voornaam : 'Evi', achternaam:'Van Looy', leeftijd: 21, email: 'evi@vanlooy.com'},
            {id: 5, voornaam : 'Luc', achternaam:'Doumen', leeftijd: 51, email: 'luc.doumen@pxl.be'},
            {id: 6, voornaam : 'Linus', achternaam:'Torvalds', leeftijd: 47, email: 'linus@linux.com'}
        ];

		// Methodes worden rechtstreeks op 'this' gedefinieerd.
		// Kijk naar 0501_persoonFactory.js, hier wordt een leeg Factory-object aangemaakt
		// en dan wordt er vervolgens met factory.getPersons gewerkt om de API te definiëren.
		this.getPersonen = function () {
			return lijstMetPersonen;
		};

		this.getPersoon = function (id) {
			return lijstMetPersonen[id - 1]; // -1 omdat de array zero-based is
		};

		this.addPersoon = function (newPersoon) {
			var id = 0;
			for (var i = 0; i < lijstMetPersonen.length; i++) {
				if (lijstMetPersonen[i].id >= id) {
					id = lijstMetPersonen[i].id + 1;
				}
			}
			newPersoon.id = id;
			lijstMetPersonen.push(newPersoon);
		}
	}
})();

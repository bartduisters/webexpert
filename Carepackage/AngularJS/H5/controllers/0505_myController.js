// myController.js
(function () {
    'use strict';

    angular.module('myApp')
        .controller('myController', myController);

    // Globals en de Value injecteren
    myController.$inject = ['GLOBALS', 'naamVanBedrijf'];
    function myController(GLOBALS, naamVanBedrijf) {
        var vm = this,
            ingelogd = false;

        // Gegevens uit de constanten ophalen en toekennen aan variabelen
        vm.appNaam = GLOBALS.appNaam;
        vm.appVersie = GLOBALS.appVersie;
        vm.isIngelogd = ingelogd ? GLOBALS.loginstatus.loguit : GLOBALS.loginstatus.login;
        // naamVanBedrijf is een Value
        vm.naamVanBedrijf = naamVanBedrijf;
    }
})();

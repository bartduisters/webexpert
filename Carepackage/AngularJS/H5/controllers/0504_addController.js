// addController.js
(function () {
	'use strict';

	angular.module('myApp')
		.controller('addController', addController);

	// Nu wordt de service gebruikt om personen toe te voegen.
	addController.$inject = ['persoonService', '$location'];

	function addController(persoonService, $location) {
		var vm = this;

		vm.addPersoon = function () {
			var newPersoon = {
				voornaam: vm.persoon.voornaam,
				achternaam: vm.persoon.achternaam,
				leeftijd: vm.persoon.leeftijd,
				email: vm.persoon.email
			};

			persoonService.addPersoon(newPersoon);

			$location.path('/');
		}
	}
})();

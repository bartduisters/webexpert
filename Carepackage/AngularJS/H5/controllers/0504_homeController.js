// homeController.js
(function () {
	'use strict';

	angular.module('myApp')
		.controller('homeController', homeController);

	// Service injecteren (in plaats van Factory).
	homeController.$inject = ['persoonService'];
	function homeController(persoonService) {
		var vm = this;

		// Gegevens uit de factory ophalen en toekennen aan variabelen.
		vm.personen = persoonService.getPersonen();
	}
})();

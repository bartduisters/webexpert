// detailController.js
(function () {
	'use strict';

	angular.module('myApp')
		.controller('detailController', detailController);

	// $routeParams wordt nu geïnjecteerd, hiermee kunnen we de /:id opvragen die je kan
	// terugvinden bij 0502_app.js.
	detailController.$inject = ['$routeParams', 'persoonFactory'];

	function detailController($routeParams, persoonFactory) {
		var vm = this,
			id = $routeParams.id;
		vm.persoon = persoonFactory.getPersoon(id);
	}
})();


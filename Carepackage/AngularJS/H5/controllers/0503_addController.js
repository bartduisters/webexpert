// addController.js
(function () {
	'use strict';
	
	angular.module('myApp')
		.controller('addController', addController);

	addController.$inject = ['persoonFactory', '$location'];
	
	function addController(persoonFactory, $location) {
		var vm = this;

		// 1. Nieuwe persoon maken op basis van het formulier (0503_add.html).
		// ID is niet bekend, deze wordt in de factory toegekend.
		vm.addPersoon = function () {
			var newPersoon = {
				voornaam: vm.persoon.voornaam,
				achternaam: vm.persoon.achternaam,
				leeftijd: vm.persoon.leeftijd,
				email: vm.persoon.email
			};

			// 2. Toevoegen via factory
			persoonFactory.addPersoon(newPersoon);

			// 3. Terug/doorsturen naar de homepage
			$location.path('/home');
		}
	}
})();

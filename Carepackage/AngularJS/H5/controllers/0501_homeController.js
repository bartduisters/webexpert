// homeController.js
(function () {
	'use strict';
	// 1. Controller toevoegen aan de module.
	angular.module('myApp')
		.controller('homeController', homeController);

	// 2. Factory injecteren.
	homeController.$inject = ['persoonFactory'];

	function homeController(persoonFactory) {
		var vm = this;

		// Gegevens ophalen uit de factory d.m.v. de API die gedefinieerd staat in 0501_persoonFactory.js.
		// Dit kennen we toe aan een variabele die beschikbaar is in de controller.
		vm.personen = persoonFactory.getPersonen();
	}
})();

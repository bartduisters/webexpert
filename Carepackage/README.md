# 1. VOER DIT UIT VOOR HET EXAMEN #

## 1.1 git clone ##
* Voer een git clone uit van deze repository in een lokale server
    * Een voorbeeld hiervan is 'htdocs' binnen lamp, xampp ...
        * De oefeningen staan per hoofdstuk.
        * De oefeningen zijn gebaseerd op 'Codevoorbeelden' uit de les.
        * Alle overtollige code is weggehaald.
        * Sommige oefeningen zijn samengevoegd, dit reflecteert in de naamgeving.
        * Alles is voorzien van commentaar.

## 1.2 npm install ##
* Voer vanuit een terminal of command prompt een 'npm install' uit, doe dit vanuit de map 'Carepackage'
* Dit installeert:
    * "angular": "1.6.1",
    * "angular-route": "1.6.1",
    * "angular-sanitize": "1.6.1",
    * "bootstrap": "3.3.7"
    

# 2. VOER DIT UIT WANNEER ER TIJDENS HET EXAMEN IETS NIET WERKT #
## 2.1 Checklist ##
* Open het bestand AngularJS_Checklist.pdf

# 3. Overzicht hoofdstukken #
## 3.1 AngularJS ##
* H1
    * AngularJS en Bootstrap inladen
* H2
    * ng-app
    * ng-model
    * ng-repeat
    * filters
* H3
    * module maken
    * controller maken
    * filters koppelen
    * ng-click
    * ng-click in ng-repeat
    * ViewModel (vm) i.p.v. $scope
* H4
    * Routing (ngRoute)
    * Master-Detail modulair
    * ng-class dynamisch veranderen
* H5
    * Factory
    * Service
    * Constants/Value
* H6
    * $http
    * ngSanatize
    * ng-bind-html
    * externe API
    * .then chaining
* H7
    * ng-hide/ng-show
    * ng-if
    * ng-class
    * ng-click
    * ng-mousemove
    * ng-keypress
    * ng-paste
    * ng-submit
    * ng-change
    * ng-options
    * ng-cloak
    * ng-focus/ng-blur
    * ng-disabled
* H8
    * custom directives (niet verwerkt in Carepackage, zou niet op examen komen)
    
## 3.2 NodeJS ##
* H1
    * Introductie
* H2
    * Node installeren
        * Linux: https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions installeer
        vanuit deze instructies. Debian/Ubuntu kijk bij 'v7.x'
    * Controleer of het gelukt is door 'node -v' (zonder quotes) in de terminal te typen
    * Voer een script uit door 'node naamvanscript.js' (zonder quotes) in de terminal te typen
    * Server starten
    * Verschillende Content-Type als header
    * Routing
* H3
    * Eigen logger maken
    * Via 'npm init' een eigen package.json maken
        * Of indien package.json al bestaat 'npm install' doen om de dependencies te installeren
* H4
    * Tijdsduur van een handeling loggen
    * Interval (om x aantal seconden iets doen)
    * Path (module)
    * File System (module)
    * Server maken die HTML-pagina's serveert
        * Mime (module)
* H5
    * Installeer bower: https://bower.io/#install-bower
        * Bower is een package manager het web
            * .bowerrc bevat de locatie waarin de dependencies geïnstalleerd moeten worden, standaard is dit bower_components
            * bower.json bevat welke dependencies er geïnstalleerd moeten worden, deze installeert ze op de locatie gedifnieerd in .bowerrc
        * Het enige dat jullie moeten doen is: bower install, vanuit de directory waar bower.json staat.
            * Dit installeert alle nodige dependencies op de aangegeven locatie. Indien de Locatie niet bestaat, wordt deze aangemaakt.
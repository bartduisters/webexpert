// homeController.js
(function () {
	'use strict';
	angular.module('myApp')
		.controller('homeController', homeController);

	// 2. Factory injecteren
	homeController.$inject = ['carFactory'];
	function homeController(carFactory) {
		var vm = this;
		vm.cars = [];
		vm.types = [];
		vm.errorMsg = '';
		vm.status = '';

		// Gegevens uit de factory ophalen en toekennen aan variabelen
		carFactory.getCars()
			.success(function (records) {
				vm.cars = records;
			})
			.error(function (err, status) {
				vm.errorMsg = err;
				vm.status = status;
			});

		carFactory.getTypes()
			.success(function (records) {
				vm.types = records;
			})
			.error(function (err, status) {
				vm.errorMsg = err;
				vm.status = status;
			});

		// Functie om via ng-click aan te roepen
		vm.deleteCar = function (id) {
			console.log("Home Controller: " + id);
			// Wissen via factory
			carFactory.deleteCar(id);
		}
	}
})();

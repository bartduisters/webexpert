// carFactory.js
(function () {
	'use strict';

	// Definitie van de carFactory
	angular.module('myApp')
		.factory('carFactory', carFactory);

	carFactory.$inject = ['$http', 'GLOBALS'];
	// Implementatie van carFactory
	function carFactory($http, GLOBALS) {
		// 1. Definieer (leeg) factory-object
		var factory = {};

		// 2. Enkel getCars maakt gebruik van
		var carsLocal = [
			{id: 1, description: 'A 180', price: 24242, type: 'A', verkeersbelasting: 250, belasting: 500, verbruik: 5.20},
			{id: 2, description: 'A 180 CDI BlueEFFICIENCY', price: 26015, type: 'A', verkeersbelasting: 250, belasting: 500, verbruik: 5.20},
			{id: 3, description: 'B 200 CDI BlueEFFICIENCY', price: 30129, type: 'B', verkeersbelasting: 250, belasting: 500, verbruik: 5.20},
			{id: 4, description: 'C 250 CGI 4MATIC BlueEFFICIENCY', price: 40414, type: 'C', verkeersbelasting: 250, belasting: 500, verbruik: 5.20},
			{id: 5, description: 'C 300 CDI 4MATIC BlueEFFICIENCY', price: 48642, type: 'C', verkeersbelasting: 250, belasting: 500, verbruik: 5.20},
			{id: 6, description: 'C 350 CGI 4MATIC BlueEFFICIENCY', price: 50941, type: 'C', verkeersbelasting: 250, belasting: 500, verbruik: 5.20},
			{id: 7, description: 'CL 500 CDI 4MATIC BlueEFFICIENCY', price: 129954, type: 'CL', verkeersbelasting: 250, belasting: 500, verbruik: 5.20},
			{id: 8, description: 'CL 600', price: 170489, type: 'CL'}
		];
		
		// 3. Defineer een API met methods voor deze factory
		factory.getCars = function () {
			return $http({
				method: 'GET',
				url: GLOBALS.carsUrl
			});
		}

		factory.getTypes = function () {
			return $http({
				method: 'GET',
				url: GLOBALS.typesUrl
			})
		}
		factory.getCarsLocal = function () {
			return carsLocal;
		}

		factory.getCar = function (id) {
			return carsLocal[id - 1]; // -1 omdat de array zero-based is
		}

		factory.deleteCar = function (id) {
			console.log(cars);
			return cars.splice(id, 1);
		}

		factory.addCar = function (newCar) {
			// De nieuwe auto heeft nog geen ID. Nu Quick&Dirty de hoogste huidige ID
			// uit de array opzoeken en deze toekennen aan newCar
			var id = 0;
			for (var i = 0; i < cars.length; i++) {
				if (cars[i].id >= id) {
					id = cars[i].id + 1;
				}
			}
			newCar.id = id;
			cars.push(newCar);
		}

		// 4. Tot slot: retourneer factory-object
		return factory;
	}
})();

// homeController.js
(function () {
	'use strict';
	angular.module('myApp')
		.controller('homeController', homeController);

	function homeController() {
		var vm = this;
		vm.cars = [
			{id: 1, description: 'A 180', price: 24242, type: 'A'},
			{id: 2, description: 'A 180 CDI BlueEFFICIENCY', price: 26015, type: 'A'},
			{id: 3, description: 'B 200 CDI BlueEFFICIENCY', price: 30129, type: 'B'},
			{id: 4, description: 'C 250 CGI 4MATIC BlueEFFICIENCY', price: 40414, type: 'C'},
			{id: 5, description: 'C 300 CDI 4MATIC BlueEFFICIENCY', price: 48642, type: 'C'},
			{id: 6, description: 'C 350 CGI 4MATIC BlueEFFICIENCY', price: 50941, type: 'C'},
			{id: 7, description: 'CL 500 CDI 4MATIC BlueEFFICIENCY', price: 129954, type: 'CL'},
			{id: 8, description: 'CL 600', price: 170489, type: 'CL'}
		];
	}
})();
